<?php

defined('APPLICATION_MODE') or define('APPLICATION_MODE', isset($_SERVER['APPLICATION_MODE'])? $_SERVER['APPLICATION_MODE']: 'production');
$configFileName = realpath(__DIR__ . DIRECTORY_SEPARATOR . '.env.php');

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

return array_replace_recursive(
	require($configFileName),
	[
		'id'                  => 'basic-console',
		'basePath'            => dirname(__DIR__),
		'bootstrap'           => ['log', 'gii'],
		'controllerNamespace' => 'app\commands',
		'modules'             => [
			'gii' => 'yii\gii\Module',
		],
		'components'          => [
			'cache' => [
				'class' => 'yii\caching\FileCache',
			],
			'log'   => [
				'targets' => [
					[
						'class'  => 'yii\log\FileTarget',
						'levels' => ['error', 'warning'],
					],
				],
			],
		],
	]
);
