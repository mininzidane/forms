<?php

return [
	'components' => [
		'db' => [
			'class'      => 'yii\db\Connection',
			'dsn'        => 'oci:dbname=localhost:1521/xe;charset=utf8',
			'username'   => 'system',
			'password'   => 'manager',
//			'charset'  => 'utf8',
			'attributes' => [
				PDO::ATTR_STRINGIFY_FETCHES => true, // параметр, необходимый для работы с CLOB полями
			],
		],
	],
	'params'     => [

	],
];
