<?php
// DI
\Yii::$container->setSingleton('app\models\reports\Storage', 'app\models\reports\impl\StorageImpl');

$config = [
	'id'         => 'skeleton',
	'basePath'   => dirname(__DIR__),
	'bootstrap'  => ['log'],
	'components' => [
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'cF-8JH7_9L7SZIDGEC0RbpEPUtg9PfJ0',
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'loginUrl'        => ['main/login'],
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'main/error',
		],
		'mailer'       => [
			'class'            => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log'          => [
			'traceLevel' => YII_DEBUG? 3: 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'urlManager'   => [
			'cache'               => null,
			'suffix'              => '/',
			'enableStrictParsing' => true,
			'class'               => 'yii\web\UrlManager',
			'enablePrettyUrl'     => true,
			'showScriptName'      => false,
			'rules'               => require('_routes.php'),
			'hostInfo'            => 'http://reports.dev.cbi-cd.ru',
		],
		'assetManager' => [
			'bundles' => [
				'yii\bootstrap\BootstrapAsset' => [
					'css' => []
				],
			],
		],
	],
	'aliases' => [
		'@views' => '@app/views',
		'@parts' => '@views/_parts',
	],
	'params'     => [
		'instagramLink' => '#',
		'emailFrom'     => 'no-reply@formsvl.ru',
		'emailTo'       => 'mininzidane@mail.ru',
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
