<?php

return [
	'/'               => 'main/index',
	'/<action>/'      => 'main/<action>',
	'/ajax/<action>/' => 'ajax/<action>',

	// PDF links
	[
		'pattern' => '/pdf/Orange_collection.pdf',
		'route'   => 'pdf/orange',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/Silver_Home_2015.pdf',
		'route'   => 'pdf/silver',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/White_collection.pdf',
		'route'   => 'pdf/white',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/VOX.pdf',
		'route'   => 'pdf/vox',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/paged-1.pdf',
		'route'   => 'pdf/paged-1',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/paged-2.pdf',
		'route'   => 'pdf/paged-2',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/cilek.pdf',
		'route'   => 'pdf/cilek',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/bionica.pdf',
		'route'   => 'pdf/bionica',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/art-cube.pdf',
		'route'   => 'pdf/art-cube',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/MediterraneanSea.pdf',
		'route'   => 'pdf/mediterranean-sea',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/sits.pdf',
		'route'   => 'pdf/sits',
		'suffix'  => '',
	],
	[
		'pattern' => '/pdf/vox-3d.pdf',
		'route'   => 'pdf/vox-3d',
		'suffix'  => '',
	],
];
