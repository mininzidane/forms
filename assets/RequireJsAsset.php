<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Vladimir Shushkov <vladimir@shushkov.ru>
 */
class RequireJsAsset extends AssetBundle {

	public $sourcePath = '@bower';

	public $baseUrl = '@web';

	public $js = [
		'requirejs/require.js',
	];

	public $css = [

	];

	public function registerAssetFiles($view) {
		$manager = $view->getAssetManager();
		$this->jsOptions = [
			'id'                   => 'main-js',
			'data-assets-base-url' => $manager->getAssetUrl($this, ''),
			'data-main'            => $manager->getAssetUrl(new AppAsset(), 'js/init')
		];
		parent::registerAssetFiles($view);
	}
}