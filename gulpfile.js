/**
 * Return url from project root from relative url from public folder
 *
 * @param {string} relativePath
 * @returns {string}
 */
var getPathFromPublic = function (relativePath) {
	return 'web/' + relativePath;
};

/**
 * vendor js and css files, define type of file by its extension
 *
 * @type {object}
 */
var vendors = {
	//'jquery': {
	//	path: 'protected/vendor/bower/jquery/dist/',
	//	files: ['jquery.js']
	//},
	//'bootstrap': {
	//	path: 'protected/vendor/bower/bootstrap/dist/css/',
	//	files: ['bootstrap.css']
	//}
};

/**
 * Get vendor files array by file extension
 *
 * @param {string} fileExtension
 * @returns {Array}
 */
var getVendorPaths = function (fileExtension) {
	var files = [];

	for (var paths in vendors) {
		if (!vendors.hasOwnProperty(paths)) {
			continue;
		}

		if (vendors[paths].files) {
			for (var i = 0; i < vendors[paths].files.length; i++) {
				// search for ending with '.{3}' (total 4 symbols)
				if (vendors[paths].files[i].indexOf('.' + fileExtension, vendors[paths].files[i].length - 4) == -1) {
					continue;
				}

				files.push(vendors[paths].path + vendors[paths].files[i]);
			}
		}
	}
	console.log('vendors ' + fileExtension + ': ' + files);
	return files;
};

// -- init plugins
var gulp = require('gulp');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var del = require('del');
var gCallback = require('gulp-callback');
// -- -- -- --

// -- init tasks into functions to run them without duplication
var tasks = {};
tasks.sass = function() {
	// clear compiled sass files before compiling because gulp-compass does not update existing files
	del([getPathFromPublic('css/sass/compiled/*')], function (err, deletedFiles) {
		console.log('Files deleted:', deletedFiles.join(', '));
	});

	return gulp
		.src(getPathFromPublic('css/sass/main.scss'))
		.pipe(compass({
			project: __dirname,
			//config_file: getPathFromPublic('config.rb'), // it could be unnecessary
			image: getPathFromPublic('images'),
			css: getPathFromPublic('css/sass/compiled'),
			//sourcemap: true, // todo add concatted source map support
			sass: getPathFromPublic('css/sass')
		}))
		.pipe(gulp.dest(getPathFromPublic('css/sass/compiled')));
};

tasks.css = function() {
	return gulp
		.src(getVendorPaths('css').concat([getPathFromPublic('css/sass/compiled/main.css')]))
		.pipe(concat('main.css'), {newLine: '\n/* ============================ */\n\n'})
		.pipe(gulp.dest(getPathFromPublic('css')));
};

tasks.js = function () {
	return gulp
		.src(getVendorPaths('js').concat([
			// add your js here: use getPathFromPublic() to get path from public
			getPathFromPublic('js/source/main.js')
		]))
		.pipe(concat('main.js', {newLine: '\n/* ============================ */\n\n'}))
		.pipe(gulp.dest(getPathFromPublic('js')));
};
// -- -- -- --

// sass/compass compiler (use 'gulp sass')
gulp.task('sass', function () {
	tasks.sass();
});

// concat sass compiled css with vendor files
gulp.task('css', function () {
	console.log(getVendorPaths('css'));

	tasks.sass().pipe(
		gCallback(function () {
			tasks.css();
		})
	);
});

// task for preparing js
gulp.task('js', function () {
	console.log(getVendorPaths('js'));

	tasks.js();
});

// dev gulp task (by default) without js and css minifies
gulp.task('default', function () {
	tasks.css();
	tasks.js();
});

// watcher for default task
gulp.task('watch', function () {
	return watch([
		'gulpfile.js',
		getPathFromPublic('css/sass/**/*.scss'),
		getPathFromPublic('js/source/*.js')
	], function () {
		tasks.sass().pipe(
			gCallback(function () {
				tasks.css();
			})
		);
		tasks.js();
	});
});

// production gulp task with minifies
gulp.task('prod', function () {
	// minify concated main.css file
	tasks.css().pipe(
		gCallback(function () {
			gulp
				.src(getPathFromPublic('css/main.css'))
				.pipe(minify())
				.pipe(gulp.dest(getPathFromPublic('css')));
		})
	);


	// uglify concated main.js file
	tasks.js().pipe(
		gCallback(function () {
			gulp
				.src(getPathFromPublic('js/main.js'))
				.pipe(uglify())
				.pipe(gulp.dest(getPathFromPublic('js')));
		})
	);
});
