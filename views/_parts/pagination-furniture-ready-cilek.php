<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/furniture-ready-cilek'],
	['main/furniture-ready-cilek-about'],
	['main/furniture-ready-cilek-map'],
	['main/furniture-ready-cilek-years'],
	['main/furniture-ready-tct-nanotec'],
];

echo $this->render('@parts/pagination', [
	'menu' => $menu,
]);
