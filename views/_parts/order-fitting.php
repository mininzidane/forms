<a href="#" class="order-fitting"
   data-toggle="modal" data-target="#orderFitting">
	<span title="Заказать&nbsp;примерку&nbsp;мебели" data-toggle="tooltip" data-placement="top" class="order-fitting__tooltip">
	</span>
</a>

<div class="modal fade" id="orderFitting">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Заказать примерку мебели</h4>
			</div>
			<div class="modal-body">
				<form action="<?= \yii\helpers\Url::to(['ajax/send-email']) ?>" class="js-ajax-submit form-callback form-callback_green">
					<div class="row">
						<div class="col-sm-6 form-group">
							<input class="form-control form-callback__text" type="text" name="phone" placeholder="Телефон">
						</div>
						<div class="col-sm-6 form-group">
							<input class="form-control form-callback__text" type="text" name="name" placeholder="Имя">
						</div>
					</div>

					<button class="btn btn-summary form-callback__button">Заказать</button>
				</form>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->