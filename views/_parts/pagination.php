<?php
/**
 * Pagination for intro pages
 *
 *
 * @var array $menu
 */

use yii\helpers\Html;
?>

<ul class="pagination">
	<?php foreach ($menu as $url) {
		echo Html::tag('li', Html::a('', $url), (Yii::$app->controller->route == $url[0]? ['class' => 'active']: []));
	} ?>
</ul>
