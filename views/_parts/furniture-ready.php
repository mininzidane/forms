<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/furniture-ready'],
	['main/furniture-ready-about'],
];

echo $this->render('@parts/pagination', [
	'menu' => $menu,
]);
