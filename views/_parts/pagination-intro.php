<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/index'],
	['main/catalog'],
	['main/benefits'],
	['main/what-to-know'],
	['main/about'],
	['main/how-we-work'],
];

echo $this->render('@parts/pagination', [
	'menu'   => $menu,
]);
