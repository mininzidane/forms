<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/furniture-ready-paged'],
	['main/furniture-ready-paged-about'],
];

echo $this->render('@parts/pagination', [
	'menu' => $menu,
]);
