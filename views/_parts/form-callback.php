<?php
/**
 * @var string $additionalClass
 * @var string $style
 */

$additionalClass = isset($additionalClass)? " $additionalClass": '';
$class = isset($style)? " form-callback_{$style}": '';
?>

<form action="<?= \yii\helpers\Url::to(['ajax/send-email']) ?>" class="js-ajax-submit form-callback<?= $class . $additionalClass ?>" method="post">
	<div class="row">
		<div class="col-sm-6 form-group">
			<input class="form-control form-callback__text" type="text" name="phone" placeholder="Телефон" required>
		</div>
		<div class="col-sm-6 form-group">
			<input class="form-control form-callback__text" type="text" name="name" placeholder="Имя">
		</div>
	</div>

	<button class="btn btn-summary form-callback__button">Заказать звонок</button>
</form>