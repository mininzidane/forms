<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/sits'],
	['main/sits-about'],
];

echo $this->render('@parts/pagination', [
	'menu' => $menu,
]);
