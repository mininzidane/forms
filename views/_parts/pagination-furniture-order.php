<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/furniture-order'],
	['main/furniture-order-about'],
];

echo $this->render('@parts/pagination', [
	'menu' => $menu,
]);
