<?php
/**
 * Pagination for intro pages
 *
 * @var \yii\web\View $this
 */

$menu = [
	['main/kitchens-pdf'],
	['main/kitchens-intro'],
	['main/kitchens-benefits'],
	['main/kitchens-tv'],
	['main/kitchens-map'],
];

echo $this->render('@parts/pagination', [
	'menu'   => $menu,
]);
