<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title, false) ?></title>
	<?php $this->head() ?>
	<script src="https://api-maps.yandex.ru/1.1/index.xml?loadByRequire=1"></script>
</head>

<?php
$phone    = '+7 423 20 90 999';
$mainMenu = [
	['label' => 'Новый Формат', 'url' => ['main/index']],
	['label' => 'Кухни на заказ', 'url' => ['main/kitchens-pdf']],
	['label' => 'Корпусная мебель: готовые решения', 'url' => ['main/furniture-ready'], 'prependText' => '(гостиные, детские, гардеробные, спальни)'],
	['label' => 'Корпусная мебель: на заказ', 'url' => ['main/furniture-order'], 'prependText' => '(гостиные, детские, гардеробные, спальни)'],
	['label' => 'Мягкая мебель', 'url' => ['main/sits']],
	['label' => 'Дарим всем!', 'url' => ['main/actions']],
	['label' => 'Продажа выставочных образцов', 'url' => ['main/sample-sales']],
	['label' => 'Дизайнерам посвящается', 'url' => ['main/designers']],
	['label' => 'Оптовые продажи', 'url' => ['main/wholesale']],
	['label' => 'Работа мечты', 'url' => ['main/dream-job']],
	['label' => 'Контакты', 'url' => ['main/contacts']],
];

$mainMenuChapters = [
	Url::to(['main/index']) => [
		Url::to(['main/catalog']),
		Url::to(['main/benefits']),
		Url::to(['main/what-to-know']),
		Url::to(['main/about']),
		Url::to(['main/how-we-work']),
	],
	Url::to(['main/kitchens-pdf']) => [
		Url::to(['main/kitchens-intro']),
		Url::to(['main/kitchens-benefits']),
		Url::to(['main/kitchens-tv']),
		Url::to(['main/kitchens-map']),
	],
	Url::to(['main/furniture-ready']) => [
		Url::to(['main/furniture-ready-about']),
		Url::to(['furniture-ready-paged']),
		Url::to(['main/furniture-ready-paged-about']),
		Url::to(['main/furniture-ready-cilek']),
		Url::to(['main/furniture-ready-cilek-about']),
		Url::to(['main/furniture-ready-cilek-map']),
		Url::to(['main/furniture-ready-cilek-years']),
		Url::to(['main/furniture-ready-tct-nanotec']),
	],
	Url::to(['main/furniture-order']) => [
		Url::to(['main/furniture-order-about']),
	],
	Url::to(['main/sits']) => [
		Url::to(['main/sits-about']),
	],
	Url::to(['main/sample-sales']) => [],
	Url::to(['main/designers']) => [],
	Url::to(['main/wholesale']) => [],
	Url::to(['main/dream-job']) => [],
	Url::to(['main/contacts']) => [],
	Url::to(['main/actions']) => [],
];
?>

<body data-main-menu-chapters='<?= json_encode($mainMenuChapters) ?>'>
<?php $this->beginBody() ?>

<ul class="js-ajax-loader spinner">
	<li></li>
	<li></li>
	<li></li>
	<li></li>
</ul>
<div id="faded" class="faded"></div>

<div class="main-menu-bg row h100 catalog" style="display: none" data-menu-target>
	<div class="hidden-xs col-sm-6 h100 main-menu-bg__shadow"></div>
	<div class="col-xs-12 col-sm-6 h100 main-menu-bg__container">
		<div class="main-menu__show" style="display: none" data-menu-target>
			<div class="main-menu__show-header">
				<div class="main-menu__credit">
					<i></i>
					<a href="<?= Url::to(['main/credit']) ?>">Рассрочка <br> от&nbsp;салона &laquo;Forms&raquo;</a>
				</div>

				<span class="main-menu__phone"><?= $phone ?></span>
				<a class="js-menu-toggle main-menu__toggle glyphicon glyphicon-remove" href="#"></a>
			</div>

			<ul class="main-menu__menu js-main-menu">
				<?php foreach ($mainMenu as $item) {
					$itemUrl = Yii::$app->request->url;
					if (!array_key_exists($itemUrl, $mainMenuChapters)) {
						foreach ($mainMenuChapters as $chapter => $chapterItems) {
							if (in_array($itemUrl, $chapterItems)) {
								$itemUrl = $chapter;
								break;
							}
						}
					}

					$options = [];
					if (Url::to($item['url']) == $itemUrl) {
						$options = ['class' => 'active'];
					}

					$prependText = isset($item['prependText'])? Html::tag('span', $item['prependText']): '';
					echo Html::tag('li', $prependText . Html::a($item['label'], $item['url']), $options);
				} ?>
			</ul>

			<p class="black-text">Мебельный салон &laquo;Forms&raquo;</p>

			<p class="black-text"><i class="ico-map"></i> г.&nbsp;Владивосток ул.&nbsp;Ильичева, 4 (ЖК&nbsp;&laquo;Аркада
				Хаус&raquo;)</p>

			<?= $this->render('@parts/contacts') ?>

			<a class="insta-folio" href="<?= Yii::$app->params['instagramLink'] ?>" target="_blank">
				Наши работы в Instagramm
			</a>
		</div>
	</div>
</div>

<div class="container container_menu">
	<a href="/" class="logo"></a>

	<div class="main-menu">
		<span class="main-menu__phone"><?= $phone ?></span>
		<a class="js-menu-toggle main-menu__toggle glyphicon glyphicon-menu-hamburger" href="#"></a>
	</div>
</div>

<div class="container container_main" id="app">
	<?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
