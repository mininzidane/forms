<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Корпусная мебель:<br> На заказ';
?>

<?= $this->render('@parts/pagination-furniture-order') ?>

<script>
	document.body.className = 'furniture-ready__bg-brown hide-logo';
</script>

<div class="row furniture-ready-cilek h100">
	<div class="col-sm-6 h100">
		<div class="h50 expand-to-gutters">
			<div id="carousel-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="/images/salon-bg.png" alt="">
					</div>
					<div class="item">
						<img src="/images/kitchen.png" alt="">
					</div>
				</div>

				<!-- Controls -->
				<a class="carousel-nav carousel-nav_left" href="#carousel-generic" role="button" data-slide="prev">
				</a>
				<a class="carousel-nav" href="#carousel-generic" role="button" data-slide="next">
				</a>
			</div>
		</div>

		<div class="kitchens-intro__choose white h50">
			<h3>Выберите уникальный дизайн вашего дома сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback', ['style' => 'white']) ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<h1 class="furniture-ready-cilek__title">
			<?= $this->title ?>
		</h1>

		<ul class="list">
			<li>Более 20 лет на&nbsp;рынке</li>
			<li>Самые смелые идеи. Любые формы, любые размеры, любые цвета</li>
			<li>Итальянский дизайн</li>
			<li>Современные технологии производства</li>
			<li>Более 100&nbsp;000 реализованных проектов</li>
		</ul>

		<div class="furniture-ready__ok furniture-order__bionica">
			<h4>Bionica</h4>
			Характерное для бионики стремление подражать естесственной
			природе, определяет главенство плавных линий и&nbsp;преобладание
			сглаженных форм. Варианты композиционных решений
			спроектированы так, чтобы доставить пользователю
			максимальный комфорт
		</div>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/sits']) ?>" class="text-horizontal-arrow uc">
				<span class="arrowed-link__arrow"></span>
				Мягкая мебель SITS
			</a>
		</div>
	</div>
</div>