<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Красивая и&nbsp;качественая<br> мебель по&nbsp;справедливой цене';
?>

<?= $this->render('@parts/pagination-intro') ?>

<script>
	document.body.className = 'index-bg';
</script>

<div class="row">
	<div class="col-sm-8 col-xs-12">
		<div class="index-logo">
			<i class="index-logo__logo"></i>
			<?= $this->title ?>
		</div>
	</div>
</div>

<div class="index-action">
	<h2 class="index-action__title">Дарим всем!</h2>
	<p class="index-action__text">Акция от&nbsp;салона &laquo;Forms&raquo;</p>
	<a class="index-action__link" href="#">Подробнее</a>
</div>

<div class="arrowed-link pin-to-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/catalog']) ?>">
		<span class="arrowed-link__text">Новый формат мебельных <br>
		магазинов</span>
		<span class="arrowed-link__arrow"></span>
	</a>
</div>