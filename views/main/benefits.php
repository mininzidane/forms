<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
$this->title = 'Почему Forms?';
?>

<?= $this->render('@parts/pagination-intro', [
	'active' => 3,
]) ?>

<script>
	document.body.className = 'benefits-bg';
</script>

<h1 class="title-benefits"><?= $this->title ?></h1>

<div class="row benefits">
	<div class="col-sm-4 benefits__item">
		<i class="benefits__icon"></i>

		<h2 class="benefits__title">Справедливая цена</h2>

		<p class="catalog__desc">Наши цены&nbsp;&mdash; это оптимальный
			баланс между качеством
			и&nbsp;доступностью</p>
	</div>
	<div class="col-sm-4 benefits__item">
		<i class="benefits__icon benefits__icon_exclusive"></i>

		<h2 class="benefits__title">Экслюзивность</h2>

		<p class="catalog__desc">В&nbsp;Приморском крае <br>
			нет аналогов нашей мебели</p>
	</div>
	<div class="col-sm-4 benefits__item">
		<i class="benefits__icon benefits__icon_beauty"></i>

		<h2 class="benefits__title">Красота</h2>

		<p class="catalog__desc">Архитектурный шедевр
			по&nbsp;доступной цене</p>
	</div>
</div>
<div class="row benefits">
	<div class="col-sm-4 benefits__item">
		<i class="benefits__icon benefits__icon_expert"></i>

		<h2 class="benefits__title">Экспертность и сервис</h2>

		<p class="catalog__desc">Наши специалисты окажут вам
			квалифицированную помощь, начиная
			от&nbsp;проектирования мебели и&nbsp;заканчивая ее&nbsp;установкой</p>
	</div>
	<div class="col-sm-4 benefits__item">
		<i class="benefits__icon benefits__icon_quality"></i>

		<h2 class="benefits__title">Качество</h2>

		<p class="catalog__desc">Материалы на&nbsp;100% соответствуют <br>европейским стандартам</p>
	</div>
	<div class="col-sm-4 benefits__item">
		<i class="benefits__icon benefits__icon_design"></i>

		<h2 class="benefits__title">Дизайн</h2>

		<p class="catalog__desc">Уникальная идея в&nbsp;каждом проекте</p>
	</div>
</div>

<div class="arrowed-link pin-to-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/what-to-know']) ?>">
		<span class="arrowed-link__text">Что нужно знать при покупке мебели?</span>
		<span class="arrowed-link__arrow"></span>
	</a>
	<?= $this->render('@parts/order-fitting') ?>
</div>