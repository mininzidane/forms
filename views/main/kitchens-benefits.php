<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Кухни - преимущества';
?>

<?= $this->render('@parts/pagination-kitchens') ?>

<script>
	document.body.className = 'light-body hide-logo';
</script>

<div class="row h100 padding-top-mobile">
	<div class="col-sm-6 h100">
		<div class="h50 expand-to-gutters">
			<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen="1" title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
		</div>

		<div class="kitchens-intro__choose kitchens-intro__choose_white-bg h50">
			<h3 class="violet">Выберите кухню сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback') ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<div class="row kitchens__benefits-container">
			<div class="col-sm-12 col-md-6">
				<div class="kitchens__benefits"><span>Гарантия 7 лет</span></div>
				<div class="kitchens__benefits kitchens__benefits_3"><span>Европейские стандарты качества</span></div>
				<div class="kitchens__benefits kitchens__benefits_5"><span>Салоны в&nbsp;70 городах России</span></div>
			</div>
			<div class="col-sm-12 col-md-6">
				<div class="kitchens__benefits kitchens__benefits_2"><span>15 лет на&nbsp;рынке</span></div>
				<div class="kitchens__benefits kitchens__benefits_4"><span>Кухни участвуют <br> в&nbsp;телепроектах: &laquo;Квартирный <br>вопрос&raquo;
						и&nbsp;&laquo;Дачный ответ&raquo;</span></div>
				<div class="kitchens__benefits kitchens__benefits_6"><span>Герман Греф и&nbsp;Наталья <br>Варлей сделали выбор <br>в&nbsp;пользу кухонь <br> &laquo;Спутник стиль&raquo;</span>
				</div>
			</div>
		</div>

		<h3 class="violet">Более 100 000 реализованных проектов</h3>

		<p>
			<a class="kitchens-intro__link" href="<?= Url::to(['main/kitchens-intro']) ?>"><span>&larr;</span>&nbsp;&nbsp;Кухни &laquo;Forms&raquo;</a>
		</p>
		<p>
			<a class="kitchens-intro__link" href="<?= Url::to(['main/kitchens-tv']) ?>"><span>&rarr;</span>&nbsp;&nbsp;Кухни участвуют в&nbsp;телепроектах:<br />
					&laquo;Квартирный вопрос&raquo; и&nbsp;&laquo;Дачный ответ&raquo;</a>
		</p>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/kitchens-tv']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Европейский стандарт
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>
