<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Новый формат мебельных магазинов';
?>

<?= $this->render('@parts/pagination-intro', [
	'active' => 2,
]) ?>

<style>
	.logo {
		display: none;
	}
</style>

<script>
	document.body.className = 'light-body';
</script>

<div class="row h100 catalog padding-top-mobile">
	<div class="col-sm-6 h100">
		<div class="catalog__h33 catalog__kitchen">
			<div class="catalog__inner">
				<h1 class="title">Кухни</h1>
			</div>

			<div class="catalog__pin-to-bottom">
				<a href="<?= Url::to(['main/kitchens-pdf']) ?>" class="text-horizontal-arrow">
					<span class="arrowed-link__arrow arrowed-link__arrow_right"></span>
					Комплексные интерьерные решения для всей семьи
				</a>
			</div>
		</div>
		<div class="catalog__h33 catalog__furniture-ready">
			<div class="catalog__inner">
				<h1 class="title">Корпусная мебель</h1>

				<div class="catalog__desc">(гостиные, детские, гардеробные, спальни)</div>
				<div class="catalog__desc-2">Готовые</div>

				<div class="catalog__pin-to-bottom">
					<a href="<?= Url::to(['main/furniture-ready']) ?>" class="text-horizontal-arrow">
						<span class="arrowed-link__arrow arrowed-link__arrow_right"></span>
						Комплексные интерьерные решения для всей семьи
					</a>
				</div>
			</div>
		</div>
		<div class="catalog__h33 catalog__furniture-order">
			<div class="catalog__inner">
				<h1 class="title">Корпусная мебель</h1>

				<div class="catalog__desc">(гостиные, детские, гардеробные, спальни)</div>
				<div class="catalog__desc-2">На заказ</div>

				<div class="catalog__pin-to-bottom">
					<a href="<?= Url::to(['main/furniture-order']) ?>" class="text-horizontal-arrow">
						<span class="arrowed-link__arrow arrowed-link__arrow_right"></span>
						Комплексные интерьерные решения для всей семьи
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 h100">
		<div class="catalog__right-center">
			<i class="logo-big"></i>
			<h2 class="catalog-title"><?= $this->title ?></h2>

			<p>Наша миссия&nbsp;&mdash; дать людям красивую и&nbsp;качественную мебель по&nbsp;справедливой цене. Мы&nbsp;стремимся
				не&nbsp;просто продать и&nbsp;забыть, а&nbsp;помочь выбрать, подсказать, как лучше. Даже если лучше&nbsp;&mdash;
				это дешевле, даже если лучше&nbsp;&mdash; это меньше, но&nbsp;все&nbsp;же лучше.</p>
		</div>

<!--		<div class="catalog__in-bottom">-->
		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/benefits']) ?>" class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Почему Forms?
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>