<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Корпусная мебель: Изучай, Твори, Смотри';
?>

<?= $this->render('@parts/furniture-ready') ?>

<script>
	document.body.className = 'furniture-ready__bg-brown hide-logo';
</script>

<div class="row h100 padding-top-mobile">
	<div class="col-sm-6 h100">
		<div class="expand-to-gutters">
			<div id="carousel-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="/images/salon-bg.png" alt="">
					</div>
					<div class="item">
						<img src="/images/kitchen.png" alt="">
					</div>
				</div>

				<!-- Controls -->
				<a class="carousel-nav carousel-nav_left" href="#carousel-generic" role="button" data-slide="prev">
				</a>
				<a class="carousel-nav" href="#carousel-generic" role="button" data-slide="next">
				</a>
			</div>
		</div>

		<div class="kitchens-intro__choose white h50">
			<h3>Выберите кухню сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback', ['style' => 'white']) ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<h1 class="furniture-ready__title"><?= $this->title ?></h1>

		<ul class="list">
			<li>Более 25 лет на&nbsp;рынке</li>
			<li>Продается в&nbsp;36 странах</li>
			<li>Каждая из&nbsp;коллеций создана известным дизайнером</li>
			<li>VOX- фанаты творчества, дизайна, <nobr>ноу-хау</nobr></li>
			<li>Вся продукция соответствует европейскому классу гигиеничности Е1</li>
			<li>Визуализация&nbsp;&mdash; возможность создания в&nbsp;салоне интерьера любой комнаты (спальни, гостиные, детские) с&nbsp;нуля</li>
		</ul>

		<div class="furniture-ready__ok">
			Высококачаственное ЛДСП, которое характеризуется очень высокой стойкостью к&nbsp;повреждениям, воздействию высокой температуры и&nbsp;влаги
		</div>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/furniture-ready-paged']) ?>" class="text-horizontal-arrow uc">
				<span class="arrowed-link__arrow"></span>
				Готовые решения Paged
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>
