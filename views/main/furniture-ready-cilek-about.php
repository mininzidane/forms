<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Корпусная мебель: Готовые решения';
?>

<?= $this->render('@parts/pagination-furniture-ready-cilek') ?>

<script>
	document.body.className = 'furniture-ready__bg-brown hide-logo';
</script>

<div class="row furniture-ready-cilek h100">
	<div class="col-sm-6 h100">
		<div class="h50 expand-to-gutters">
			<div id="carousel-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="/images/salon-bg.png" alt="">
					</div>
					<div class="item">
						<img src="/images/kitchen.png" alt="">
					</div>
				</div>

				<!-- Controls -->
				<a class="carousel-nav carousel-nav_left" href="#carousel-generic" role="button" data-slide="prev">
				</a>
				<a class="carousel-nav" href="#carousel-generic" role="button" data-slide="next">
				</a>
			</div>
		</div>

		<div class="kitchens-intro__choose white h50">
			<h3>Выберите креативную комнату <br>для вашего ребенка сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback', ['style' => 'white']) ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<h1 class="furniture-ready-cilek__title">
			<?= $this->title ?>
			<span class="furniture-ready-cilek__desc">(детские)</span>
		</h1>

		<ul class="list">
			<li>Продается на&nbsp;5 континентах. В&nbsp;66 странах. 444 торговые точки</li>
			<li>Начиная с&nbsp;1995 года, фабрика занимается производством детской мебели&nbsp;&mdash; это единственная из&nbsp;компаний на&nbsp;мировом рынке, которая специализированно занимается производством только детской мебели</li>
			<li>Мебель соотвествует стандартам CE, E1, TUV, GS, LGA, LSA, RoHS, Confidense in&nbsp;Textile, Ростест</li>
		</ul>

		<div class="furniture-ready__ok">
			На&nbsp;фабрике работает специальная <nobr>&laquo;Креатив-Студия&raquo;</nobr>, занимающаяся разработкой дизайна, учитывающего психологические особенности ребенка и&nbsp;его безопасность
		</div>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/furniture-ready-cilek-map']) ?>" class="text-horizontal-arrow uc">
				<span class="arrowed-link__arrow"></span>
				Особенности мебели
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>
