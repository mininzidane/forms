<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Кухни';
?>

<?= $this->render('@parts/pagination-kitchens') ?>

<script>
	document.body.className = 'light-body';
</script>

<div class="row h100 padding-top-mobile">
	<div class="col-sm-6 h100">
		<div class="kitchens-intro__about h50 block-padding-top">
			<h1 class="violet title"><?= $this->title ?></h1>
			<p>Мебельный салон Forms является эксклюзивным представителем Московской фабрики кухонь &laquo;Спутник стиль&raquo; во&nbsp;Владивостоке. &laquo;Спутник стиль&raquo; входит в&nbsp;тройку лидеров рынка кухонной мебели России.</p>
			<a class="kitchens-intro__link" href="<?= Url::to(['main/kitchens-pdf']) ?>"><span>&rarr;</span>&nbsp;&nbsp;Спутник стиль</a>
		</div>

		<div class="kitchens-intro__choose h50">
			<h3 class="violet">Выберите кухню сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback') ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<div class="kitchens-intro__circle">
			<span>более</span>
			<b>500</b>
			<span>вариантов исполнения</span>
		</div>
		<div class="kitchens-intro__circle kitchens-intro__circle_small">
			<span>более</span>
			<b>30</b>
			<span>моделей кухонь</span>
		</div>

		<div class="pin-to-bottom pin-to-bottom_w50 margin-bottom">
			<a href="<?= Url::to(['main/kitchens-benefits']) ?>" class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Европейский стандарт
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>
