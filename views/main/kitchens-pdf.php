<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Кухни Forms';
?>

<?= $this->render('@parts/pagination-kitchens') ?>

<script>
	document.body.className = 'kitchens__bg';
</script>

<h1 class="title-benefits"><?= $this->title ?></h1>

<div class="kitchens__folio-insta">
	<a href="<?= Yii::$app->params['instagramLink'] ?>" target="_blank">
		Наши работы в Instagramm
	</a>
</div>

<div class="row">
	<div class="col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/orange']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/orange.png" alt="">
			<span>Оранжевая коллекция <br>
			Современные кухни</span>
		</a>
	</div>
	<div class="col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/silver']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/silver.png" alt="">
			<span>Серебрянная коллекция <br>
			Уникальные кухни</span>
		</a>
	</div>
	<div class="col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/white']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/white.png" alt="">
			<span>Белая коллекция <br>
			Классические кухни</span>
		</a>
	</div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/kitchens-intro']) ?>">
		<span class="arrowed-link__text">Кухни Forms</span>
		<span class="arrowed-link__arrow"></span>
	</a>
	<?= $this->render('@parts/order-fitting') ?>
</div>