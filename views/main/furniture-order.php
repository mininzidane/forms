<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Корпусная мебель: на&nbsp;заказ';
?>

<?= $this->render('@parts/pagination-furniture-order') ?>

<script>
	document.body.className = 'furniture-order__bg';
</script>

<h1 class="title-benefits"><?= $this->title ?></h1>

<div class="furniture-ready__desc t-C">
	(гостиные, спальни, гардеробные, детские)
</div>

<div class="row">
	<div class="col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/bionica']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/FP-pdf-1.png" alt="">
			<span>Коллекция Bionica</span>
		</a>
	</div>
	<div class="col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/art-cube']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/FP-pdf-2.png" alt="">
			<span>Коллекция ArtCube</span>
		</a>
	</div>
	<div class="col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/mediterranean-sea']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/FP-pdf-3.png" alt="">
			<span>Коллекция Средиземноморье</span>
		</a>
	</div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/furniture-order-about']) ?>">
		<span class="arrowed-link__text">мебель Filippe Grandy</span>
		<span class="arrowed-link__arrow"></span>
	</a>
</div>