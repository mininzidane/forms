<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Корпусная мебель:<br> готовые решения';
?>

<?= $this->render('@parts/pagination-furniture-ready-cilek') ?>

<script>
	document.body.className = 'furniture-ready-cilek__bg';
</script>

<h1 class="title-benefits"><?= $this->title ?></h1>

<div class="furniture-ready__desc t-C">
	(детская мебель)
</div>

<div class="row">
	<div class="col-sm-offset-4 col-sm-4 t-C">
		<a href="<?= Url::to(['pdf/cilek']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/cilek-pdf.png" alt="">
			<span>Каталог мебели CILEK</span>
		</a>
	</div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/furniture-ready-cilek-about']) ?>">
		<span class="arrowed-link__text">Мебель CILEK</span>
		<span class="arrowed-link__arrow"></span>
	</a>
	<?= $this->render('@parts/order-fitting') ?>
</div>