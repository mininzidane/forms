<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Мягкая мебель<br> SITS';
?>

<?= $this->render('@parts/pagination-sits') ?>

<script>
	document.body.className = 'sits__bg';
</script>

<h1 class="title-benefits"><?= $this->title ?></h1>

<div class="row">
	<div class="col-sm-offset-4 col-sm-4 t-C">
		<a href="/pdf/" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/sits-pdf.png" alt="">
			<span>Мягкая мебель SITS</span>
		</a>
	</div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/sits-about']) ?>">
		<span class="arrowed-link__text">Мебель SITS</span>
		<span class="arrowed-link__arrow"></span>
	</a>
</div>