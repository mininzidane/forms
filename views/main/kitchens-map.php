<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Выпуски телепередач';
?>

<?= $this->render('@parts/pagination-kitchens') ?>

<script>
	document.body.className = 'light-body';
</script>

<div class="js-kitchen-map kitchen-map">
	<div class="map-pin kitchen-map__1"
		 title="<div class='tooltip-header'>Фасады</div> Массив дерева&nbsp;&mdash; три ведущих итальянских производителя Stival, ILCAM, Mobilclan"></div>
	<div class="map-pin kitchen-map__2"
		title="<div class='tooltip-header'>Пленка ПВХ</div> от 0,4 до 0,75 мм, более тягучая, что обеспечивает плотное прилегание. Наносится путем вакуумного пресования. Применяется в пищевой"></div>
	<div class="map-pin kitchen-map__3"
		title="<div class='tooltip-header'>ЛДСП 18 мм EGGER</div> Австрия высокая плотность плиты, нет выделений формальдегида"></div>
	<div class="map-pin kitchen-map__4"
		title="<div class='tooltip-header'>Глубина верхних шкафов - 35 см</div> Это дает возможность разместить два чайных набора в глубину"></div>
	<div class="map-pin kitchen-map__5"
		title="<div class='tooltip-header'>Ручки</div> Только металл т стекло, никакого пластика. Безопасная - завальцовка"></div>
	<div class="map-pin kitchen-map__6"
		title="<div class='tooltip-header'>Кромка</div> 1 мм Евростандарт: плотное прилегание, не отслаивается, наностится автоматически, в отличие от кромки 0,4 мм, которую приклеивают утюгом"></div>
	<div class="map-pin kitchen-map__7"
		 title="<div class='tooltip-header'>Фартук</div> В цвет столешницы - 8-10 мм"></div>
	<div class="map-pin kitchen-map__8"
		title="<div class='tooltip-header'>Фурнитура</div> Hettich(Германия) и Blum(Австрия): 25 лет гарантии, крючки навески выдерживают до 90 килограмм, в отличе от других производителей, максимальная нагрузка, на которые на 15 килограмм меньше"></div>
	<div class="map-pin kitchen-map__9"
		title="<div class='tooltip-header'>Эстетика</div> нет самоклеящихся заглушек скрывающих винты"></div>
	<div class="map-pin kitchen-map__10"
		title="<div class='tooltip-header'>Алюминиевая подложка под мойкой</div> Защита днища стола от протечек"></div>
	<div class="map-pin kitchen-map__11"
		title="<div class='tooltip-header'>Врезная задняя стенка</div> Вставляется в специально пропиленые пазы"></div>
	<div class="map-pin kitchen-map__12"
		title="<div class='tooltip-header'>Цоколь</div> Пластиковый с силиконовым уплотнителем. В классических кухнях из массива - шпонированый"></div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link text-horizontal-arrow_violet" href="<?= Url::to(['main/furniture-ready']) ?>">
		<span class="arrowed-link__text">Корпусная мебель</span>
		<span class="arrowed-link__arrow"></span>
	</a>
	<?= $this->render('@parts/order-fitting') ?>
</div>