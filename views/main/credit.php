<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Рассрочка от салона Forms-Мебель';
?>

<script>
	document.body.className = 'light-body white-body';
</script>

<h1 class="violet title title-margin-top t-C"><?= $this->title ?></h1>
<div class="credit__desc">БЕЗ УЧАСТИЯ БАНКА. БЕЗ ПРОЦЕНТОВ. БЕЗ СТРАХОВОК. БЕЗ ПЕРЕПЛАТ.</div>
<div class="credit__desc violet">Общая стоимость кухни&nbsp;&mdash; 285&nbsp;000&nbsp;руб.</div>

<div class="row">
	<div class="col-sm-6">
		<p>Рассрочка на 4 месяца</p>

		<table class="table table-condensed credit__table">
			<tr>
				<th>Дата платежа</th>
				<th>Платеж</th>
				<th>Сумма платежа, руб.</th>
				<th>Сумма остатка для досрочного погашения, руб.</th>
			</tr>
			<tr>
				<td>09.07.2015</td>
				<td>Первый взнос</td>
				<td>114 000</td>
				<td>171 000</td>
			</tr>
			<tr>
				<td>08.08.2015</td>
				<td>1-й платеж</td>
				<td>42 750</td>
				<td>171 000</td>
			</tr>
			<tr>
				<td>07.09.2015</td>
				<td>2-й платеж</td>
				<td>42 750</td>
				<td>128 250</td>
			</tr>
			<tr>
				<td>07.10.2015</td>
				<td>3-й платеж</td>
				<td>42 750</td>
				<td>85 500</td>
			</tr>
			<tr>
				<td>06.11.2015</td>
				<td>4-й платеж</td>
				<td>42 750</td>
				<td></td>
			</tr>
		</table>
		<div class="credit__total">Итого: 285 000 руб.</div>
	</div>
	<div class="col-sm-6">
		<p>Рассрочка на 6 месяца</p>
		<table class="table table-condensed credit__table">
			<tr>
				<th>Дата платежа</th>
				<th>Платеж</th>
				<th>Сумма платежа, руб.</th>
				<th>Сумма остатка для досрочного погашения, руб.</th>
			</tr>
			<tr>
				<td>09.07.2015</td>
				<td>Первый взнос</td>
				<td>114 000</td>
				<td>171 000</td>
			</tr>
			<tr>
				<td>08.08.2015</td>
				<td>1-й платеж</td>
				<td>28 500</td>
				<td>171 000</td>
			</tr>
			<tr>
				<td>07.09.2015</td>
				<td>2-й платеж</td>
				<td>28 500</td>
				<td>142 50</td>
			</tr>
			<tr>
				<td>07.10.2015</td>
				<td>3-й платеж</td>
				<td>28 500</td>
				<td>114 000</td>
			</tr>
			<tr>
				<td>06.11.2015</td>
				<td>4-й платеж</td>
				<td>28 500</td>
				<td>85 500</td>
			</tr>
			<tr>
				<td>06.12.2015</td>
				<td>5-й платеж</td>
				<td>28 500</td>
				<td>57 000</td>
			</tr>
			<tr>
				<td>05.01.2016</td>
				<td>6-й платеж</td>
				<td>28 500</td>
				<td></td>
			</tr>
		</table>
		<div class="credit__total">Итого: 285 000 руб.</div>
	</div>
</div>

<?= $this->render('@parts/button-back') ?>