<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Выпуски телепередач';
?>

<?= $this->render('@parts/pagination-kitchens') ?>

<script>
	document.body.className = 'light-body hide-logo';
</script>

<div class="row h100 padding-top-mobile">
	<div class="col-sm-6 h100">
		<div class="h50 video-container">
			<div id="winter-landscape" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="hospitable-veranda" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="scandinavian-apron" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="living-room-30-guests" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="tetris" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="farmhouse-style" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="new-york-patio" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="young-face" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
			<div id="smart-table" style="display: none" data-show-by-hash-group>
				<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
			</div>
		</div>

		<div class="kitchens-intro__choose kitchens-intro__choose_white-bg h50">
			<div class="row">
				<div class="col-sm-6">
					<h2 class="violet uc">Телепередачи &laquo;Квартирный вопрос&raquo; и&nbsp;&laquo;Дачный ответ&raquo;</h2>
					<p><a class="kitchens-intro__link" href="<?= Url::to(['main/kitchens-intro']) ?>"><span>&larr;</span>&nbsp;&nbsp;Кухни &laquo;Forms&raquo;</a></p>
					<p><a class="kitchens-intro__link" href="<?= Url::to(['main/kitchens-pdf']) ?>"><span>&larr;</span>&nbsp;&nbsp;Спутник стиль</a></p>
				</div>
				<div class="col-sm-6 t-C">
					<p>
						<img src="/images/kvar-vopros.png" alt="">
					</p>
					<p>
						<img src="/images/dach-otvet.png" alt="">
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<h1 class="violet title"><?= $this->title ?></h1>

		<ul class="kitchens__tv js-show-by-hash">
			<li>
				<a href="#winter-landscape" class="text-horizontal-arrow text-horizontal-arrow_violet active">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня с зимним пейзажем
				</a>
			</li>
			<li>
				<a href="#hospitable-veranda" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Гостеприимная веранда
				</a>
			</li>
			<li>
				<a href="#scandinavian-apron" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня в&nbsp;скандинавском фартуке
				</a>
			</li>
			<li>
				<a href="#living-room-30-guests" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня гостиная на&nbsp;30 гостей
				</a>
			</li>
			<li>
				<a href="#tetris" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня&nbsp;&mdash; тетрис
				</a>
			</li>
			<li>
				<a href="#farmhouse-style" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня в&nbsp;фермерском стиле
				</a>
			</li>
			<li>
				<a href="#new-york-patio" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					<nobr>Нью-йоркский</nobr> дворик
				</a>
			</li>
			<li>
				<a href="#young-face" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня с&nbsp;молодым лицом
				</a>
			</li>
			<li>
				<a href="#smart-table" class="text-horizontal-arrow text-horizontal-arrow_violet">
					<span class="arrowed-link__arrow arrowed-link__arrow_right arrowed-link__arrow_small"></span>
					Кухня с&nbsp;умным столом
				</a>
			</li>
		</ul>

		<div class="pin-to-bottom pin-to-bottom_w50 margin-bottom">
			<a href="<?= Url::to(['main/kitchens-map']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Европейский стандарт
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>
