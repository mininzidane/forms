<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Дизайнерам посвящается';
?>

<script>
	document.body.className = 'light-body';
</script>

<div class="row h100 padding-top-mobile">
	<div class="col-sm-6 h100 block-padding-top">
		<h1 class="violet title"><?= $this->title ?></h1>

		<p>Приглашаем к&nbsp;сотрудничеству дизайнеров и&nbsp;архитекторов для реализации уникальных и&nbsp;ярких
			творческих идей.</p>

		<p>Мы&nbsp;хотим вовлечь вас в&nbsp;интересный процесс создания индивидуального интерьера на&nbsp;базе наших
			коллекций, с&nbsp;множеством вариантов цветов, отделок и&nbsp;декоров.</p>

		<p>Помимо всего прочего в&nbsp;ассортименте нашей мебели представлены
			столы, стулья повышенной прочности для общественых помещений
			(бары, рестораны, кафе, гостиницы и&nbsp;др.)</p>

		<p>А&nbsp;также мебель для детских образовательных учреждений.</p>
	</div>
	<div class="col-sm-6 h100 block-padding-top">
		<div class="catalog-row">
			<h4 class="violet uc">Каталоги кухонь</h4>

			<div class="row">
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/orange']) ?>">
						<img src="/images/orange-mini.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/silver']) ?>">
						<img src="/images/silver-mini-2.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/white']) ?>">
						<img src="/images/white-mini.png" alt="">
					</a>
				</div>
			</div>
		</div>

		<div class="catalog-row">
			<h4 class="violet uc">Корпусная мебель: готовые решения</h4>

			<div class="row">
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/paged-1']) ?>">
						<img src="/images/paged-mini-1.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/paged-2']) ?>">
						<img src="/images/paged-mini-2.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/vox']) ?>">
						<img src="/images/vox-mini.png" alt="">
					</a>
				</div>
			</div>
		</div>

		<div class="catalog-row">
			<h4 class="violet uc">Корпусная мебель: на заказ</h4>

			<div class="row">
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/bionica']) ?>">
						<img src="/images/fp-mini-1.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/art-cube']) ?>">
						<img src="/images/fp-mini-2.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/mediterranean-sea']) ?>">
						<img src="/images/fp-mini-3.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<a href="<?= Url::to(['pdf/sits']) ?>">
						<img src="/images/sits-mini.png" alt="">
					</a>
				</div>
			</div>
		</div>

		<div class="catalog-row">
			<div class="row">
				<div class="col-xs-3">
					<h4 class="violet uc">Детские</h4>

					<a href="<?= Url::to(['pdf/cilek']) ?>">
						<img src="/images/cilek-mini.png" alt="">
					</a>
				</div>
				<div class="col-xs-3">
					<h4 class="violet uc nobr">3D модели VOX</h4>

					<a href="<?= Url::to(['pdf/vox-3d']) ?>">
						<img src="/images/3d-max.png" alt="">
					</a>
				</div>
			</div>
		</div>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/dream-job']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Работа мечты
			</a>
		</div>
	</div>
</div>
