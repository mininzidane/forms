<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Корпусная мебель: Готовые решения';
?>

<?= $this->render('@parts/furniture-ready-paged') ?>

<script>
	document.body.className = 'furniture-ready__bg-brown-light hide-logo light-brown-body';
</script>

<div class="row furniture-ready-paged h100">
	<div class="col-sm-6 h100">
		<div class="h50 expand-to-gutters">
			<div id="carousel-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="/images/salon-bg.png" alt="">
					</div>
					<div class="item">
						<img src="/images/kitchen.png" alt="">
					</div>
				</div>

				<!-- Controls -->
				<a class="carousel-nav carousel-nav_left" href="#carousel-generic" role="button" data-slide="prev">
				</a>
				<a class="carousel-nav" href="#carousel-generic" role="button" data-slide="next">
				</a>
			</div>
		</div>

		<div class="kitchens-intro__choose h50">
			<h3>Выберите мебель сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback', ['style' => 'brown']) ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<h1 class="furniture-ready-paged__title"><?= $this->title ?></h1>
		<span class="furniture-ready-paged__desc">(гостиные, спальни, гардеробные, детские)</span>

		<ul class="list list_brown">
			<li>История Paged началась с&nbsp;1881 года, с&nbsp;небольшого производства мебели из&nbsp;гнутого дерева, извесной под названием &laquo;Венские стулья&raquo;</li>
			<li>Инновационная технология изгиба древесины с&nbsp;помощью особой гидротехнической обработки&nbsp;&mdash; &laquo;визитная карточка&raquo; Paged</li>
			<li>Мебель Paged можно встретить в&nbsp;лучших домах и&nbsp;квартирах, практически всего мира&nbsp;&mdash; Великобритании, Голландии, США, Японии, Германии и&nbsp;других стран</li>
		</ul>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/furniture-ready-cilek']) ?>" class="text-horizontal-arrow text-horizontal-arrow_brown uc">
				<span class="arrowed-link__arrow"></span>
				Готовые решения Cilek
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>
