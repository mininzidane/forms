<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Выпуски телепередач';
?>

<?= $this->render('@parts/pagination-furniture-ready-cilek') ?>

<script>
	document.body.className = 'light-body';
</script>
<style>
	body {
		background-color: #f8f8f8;
	}
</style>

<div class="js-kitchen-map kitchen-map cilek-map">
	<div class="map-pin cilek-map__1"
		 title="<div class='tooltip-header'>Краска</div> Используется натуральная краска на&nbsp;водной основе, без запаха"></div>
	<div class="map-pin cilek-map__2"
		title="<div class='tooltip-header'>Металл</div> На&nbsp;металлические части нанесена защита от&nbsp;коррозии"></div>
	<div class="map-pin cilek-map__3"
		title="Нет острых углов. Обработанные кромки и заглушки"></div>
	<div class="map-pin cilek-map__4"
		title="<div class='tooltip-header'>Наклейки</div> Для наклеек используется офсетная печать, безопасная для здоровья детей"></div>
	<div class="map-pin cilek-map__5"
		title="Внутренние части шкафов оборудованы светодиодными лампами"></div>
	<div class="map-pin cilek-map__6"
		title="<div class='tooltip-header'>Ручки</div> Ручки сделаны из фарфора, исскуственной кожи и метала"></div>
	<div class="map-pin cilek-map__7"
		 title="Максимальная несущая способность рельса ящиков стола - 30 килограмм"></div>
	<div class="map-pin cilek-map__8"
		title="<div class='tooltip-header'>Пластиковые элементы в мебели Cilek</div> Это полистирол - из него изготавливают стаканчики для йогурта"></div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link text-horizontal-arrow_violet" href="<?= Url::to(['main/furniture-ready-cilek-years']) ?>">
		<span class="arrowed-link__text">Cilek на многие года</span>
		<span class="arrowed-link__arrow"></span>
	</a>
	<?= $this->render('@parts/order-fitting') ?>
</div>