<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Работа мечты';
?>

<script>
	document.body.className = 'light-body white-body';
</script>

<div class="row h100 dream-job padding-top-mobile">
	<div class="col-sm-4 h100 block-padding-top">
		<h1 class="violet title"><?= $this->title ?></h1>

		<ul class="dream-job__links">
			<li class="active"><a href="#designer" data-toggle="tab">Дизайнер</a></li>
			<li><a href="#sale-manager" data-toggle="tab">Менеджер продаж</a></li>
		</ul>

		<div>Отдел кадров:</div>
		<p class="wholesale__phone">+7 423 20 90 999</p>
		<a href="mailto:hr@formsvl.ru" class="btn form-callback__button dream-job__button">Отправить резюме</a>

		<p><strong>Наши дизайнеры не&nbsp;ведут продажи,<br />
		они предлагают решения для наших клиентов!</strong></p>

		<p>С&nbsp;готовностью ответим на&nbsp;все ваши вопросы<br />
		по&nbsp;сотрудничеству! Пишите и&nbsp;отправляйте резюме<br />
		<a href="mailto:hr@formsvl.ru" class="violet"><strong>hr@formsvl.ru</strong></a>!</p>
	</div>
	<div class="col-sm-8 h100 block-padding-top tab-content">
		<div class="tab-pane what-to-know__answer dream-job__desc active" id="designer">
			<p>Вы&nbsp;уже связали или хотите связать свою профессиональную судьбу с&nbsp;дизайном? Получить опыт
				сотрудничества с&nbsp;ведущими фабриками по&nbsp;производству мебели?</p>

			<h4>Предлагаем вам:</h4>
			<ul class="list list_black">
				<li>создавать 3D визуализации, разрабатывать дизайн&nbsp;&mdash; проекты интерьеров и&nbsp;мебели
					(корпусная)
					для наших клиентов с&nbsp;учетом современных тенденций и&nbsp;предпочтений заказчика;
				</li>
				<li>формировать техническое задание для фабрик на&nbsp;изготовление мебели;</li>
				<li>сопровождать производственный цикл заказов до&nbsp;доставки на&nbsp;склад.</li>
			</ul>

			<h4>Требования:</h4>
			<ul class="list list_black">
				<li>навыки работы с&nbsp;графическими и&nbsp;инженерными программами (Photoshop, AutoCAD, CorelDraw
					<nobr>и т. д.</nobr>
					), желательно знание
					<nobr>1 &deg;C</nobr>
					;<br/>
				<li>развитое чувство вкуса, креативность, способность находить дизайнерские решения разной степени
					сложности, умение работать на&nbsp;результат;<br/>
				<li>точность и&nbsp;внимательность в&nbsp;работе с&nbsp;цифрами, чертежами, документами, внимание к&nbsp;деталям;<br/>
					приветствуется опыт работы в&nbsp;интерьерном дизайне.
				</li>
			</ul>

			<h4>У нас есть для вас:</h4>
			<ul class="list list_black">
				<li>уютная атмосфера, отличные технические возможности и&nbsp;инструментальное оснащение;</li>
				<li>возможность профессионально развиваться и&nbsp;расширять свой кругозор (обучение, тренинги за&nbsp;счет
					компании);
				</li>
				<li>свобода для различных дизайнерских решений;</li>
				<li>стабильный уровень заработной платы + % от&nbsp;реализованных заказов;</li>
				<li>полный рабочий день, оформление в&nbsp;соответствии с&nbsp;ТК&nbsp;РФ;</li>
				<li>испытательный срок&nbsp;&mdash; до&nbsp;трех месяцев (зависит от&nbsp;результата работы)</li>
			</ul>
		</div>
		<div class="tab-pane what-to-know__answer dream-job__desc" id="sale-manager">
			<ul class="list list_black">
				<li>уютная атмосфера, отличные технические возможности и&nbsp;инструментальное оснащение;</li>
				<li>возможность профессионально развиваться и&nbsp;расширять свой кругозор (обучение, тренинги за&nbsp;счет
					компании);
				</li>
				<li>свобода для различных дизайнерских решений;</li>
				<li>стабильный уровень заработной платы + % от&nbsp;реализованных заказов;</li>
				<li>полный рабочий день, оформление в&nbsp;соответствии с&nbsp;ТК&nbsp;РФ;</li>
				<li>испытательный срок&nbsp;&mdash; до&nbsp;трех месяцев (зависит от&nbsp;результата работы)</li>
			</ul>
		</div>
	</div>
</div>
