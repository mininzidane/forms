<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Контактная информация';
?>
<!--<script src="https://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>-->
<script>
	document.body.className = 'light-body';
</script>
<style>
	body {
		background-color: #ececec;
	}
</style>

<div class="row h100">
	<div class="col-sm-6 h100">
		<h1 class="how-we-work__title title title-margin-top"><?= $this->title ?></h1>
		<p>Мебельный салон &laquo;Forms&raquo; г.&nbsp;Владивосток, ул.&nbsp;Ильичева, 4 (ЖК&nbsp;&laquo;Аркада Хаус&raquo;)<br />
		<nobr>ООО &laquo;Новый Формат&raquo;</nobr>. <acronym title="Идентификационный номер налогоплательщика" lang="ru">ИНН</acronym> 2543030932</p>
		<?= $this->render('@parts/contacts') ?>

		<div class="expand-to-gutters">
			<div id="YMapsID" style="width: 100%; height: 400px;"></div>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<div class="expand-to-gutters">
			<p>
				<img src="/images/contact-2.png" alt="" class="mw-100">
				<br>
				<img src="/images/contact-1.png" alt="" class="mw-100">
			</p>
		</div>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/sample-sales']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Продажа выставочных образцов
			</a>
		</div>
	</div>
</div>
