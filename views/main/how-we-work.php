<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Как мы работаем';
?>

<?= $this->render('@parts/pagination-intro', [
	'active' => 6,
]) ?>

<script>
	document.body.className = 'light-body';
</script>

<div class="h50">
<h1 class="how-we-work__title title title-margin-top"><?= $this->title ?></h1>

<ul class="how-we-work__scheme">
	<li class="how-we-work__scheme-step1">
		Заказ примерки мебели*
	</li>
	<li class="how-we-work__scheme-step2">
		Примерка мебели*
	</li>
	<li class="how-we-work__scheme-step3">
		Прорисовка
		<nobr>дизайн-проекта</nobr>
		и&nbsp;расчет стоимости
	</li>
	<li class="how-we-work__scheme-step4">
		Заключение
		договора и&nbsp;оплата
	</li>
	<li class="how-we-work__scheme-step5">
		Установка мебели
	</li>
</ul>
</div>

<div class="row h50">
	<div class="col-sm-6 how-we-work__green-block how-we-work__propose h100">
		<div data-callback-form-intro class="how-we-work__text">
			<p>В&nbsp;процессе создания <nobr>дизайн-проекта</nobr>, замер&nbsp;&mdash; исходная точка моделирования. Неточно выполненный замер ведет к&nbsp;дополнительным расходам и&nbsp;увеличению сроков реализации проекта, а&nbsp;неверно продуманный дизайн&nbsp;&mdash; к&nbsp;ежедневному разочарованию.</p>
			<p class="uc">Мы&nbsp;избавим вас от&nbsp;этих проблем!</p>

			<a href="#fitting-form" class="js-show-form text-horizontal-arrow text-horizontal-arrow_bold uc">
				<span class="arrowed-link__arrow arrowed-link__arrow_right"></span>
				Закажите примерку мебели бесплатно
			</a>
		</div>

		<div style="display: none" id="fitting-form">
			<h3 class="uc white">Закажите примерку мебели бесплатно!</h3>
			<p>К&nbsp;вам приедет дизайнер со&nbsp;всеми необходимыми инструментами и&nbsp;каталогами</p>

			<ul class="list">
				<li>Проведет замер помещения</li>
				<li>Поможет определиться с&nbsp;дизайном</li>
			</ul>
			<div class="row">
				<div class="col-sm-6">
					<?= $this->render('@parts/form-callback', ['additionalClass' => 'js-ajax-submit', 'style' => 'transparent']) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 how-we-work__right-block h100">
		<p>Что такое примерка мебели? Это выездная экспертная консультация специалиста с&nbsp;замером помещения</p>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/kitchens-pdf']) ?>" class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Кухни forms
			</a>
			<?= $this->render('@parts/order-fitting') ?>
		</div>
	</div>
</div>