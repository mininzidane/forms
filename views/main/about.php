<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Салон мебели &laquo;Forms&raquo;';
?>

<?= $this->render('@parts/pagination-intro') ?>

<script>
	document.body.className = 'about__bg';
</script>

<div class="row">
	<div class="col-sm-6 about">
		<div class="">
			<h1 class="about__title title"><?= $this->title ?></h1>

			<p class="color-white">Это красивый
			<nobr>шоу-рум</nobr>, площадью 700 квадратных метров, поделенный на&nbsp;тематические зоны: кухни, спальни, гостиные, детская
			мебель.
			Перед салоном расположена удобная парковка.
			Для нас каждый посетитель&nbsp;&mdash; гость, которого мы&nbsp;обязаны напоить настоящим чаем
			и&nbsp;свежесваренным кофе:) Мы&nbsp;стремимся к&nbsp;повышению осведомленности наших Гостей.
			Всегда готовы ответить на&nbsp;любые вопросы по&nbsp;материалам, фурнитуре и&nbsp;технологии изготовления
			нашей
			продукции. После посещения салона, вы&nbsp;сможете профессионально выбирать мебель без затрат
			на&nbsp;специалисов</p>

			<a href="<?= Url::to(['main/how-we-work']) ?>" class="text-horizontal-arrow">
				<span class="arrowed-link__arrow arrowed-link__arrow_right"></span>
				Как мы работаем
			</a>
		</div>
	</div>
</div>

<div class="pin-to-bottom">
	<?= $this->render('@parts/order-fitting') ?>
</div>