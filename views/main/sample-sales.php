<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Продажа собранных коллекций';
?>

<script>
	document.body.className = 'light-body';
</script>
<style>
	body {
		background-color: #f0f3f8;
	}
</style>

<div class="row h100">
	<div class="col-sm-6 h100">
		<h1 class="violet title title-margin-top"><?= $this->title ?></h1>

		<p>Прямо сейчас вы&nbsp;можете купить готовые коллекции, представленные<br/>
			в&nbsp;торговом зале мебельного салона за&nbsp;ПОЛЦЕНЫ! Почему такая скидка?<br/>
			Это компенсация за&nbsp;то, что коллекция продается только в&nbsp;скомплектованном<br/>
			виде и&nbsp;не&nbsp;подлежит корректировке</p>

		<p>Торопитесь, предложение ограничено!</p>

		<div class="expand-to-gutters">
			<div id="carousel-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="/images/salon-bg.png" alt="">
					</div>
					<div class="item">
						<img src="/images/kitchen.png" alt="">
					</div>
				</div>

				<!-- Controls -->
				<a class="carousel-nav carousel-nav_top carousel-nav_left" href="#carousel-generic" role="button" data-slide="prev">
				</a>
				<a class="carousel-nav carousel-nav_top" href="#carousel-generic" role="button" data-slide="next">
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">

		<div class="t-C padding-bottom">
			<div class="discount-circle">
				<div class="discount-circle__disc">
					<div class="discount-circle__text">Скидка</div>
					<div class="discount-circle__value">50%</div>
				</div>
				<div class="discount-circle__append">при 100% оплате</div>
			</div>
			<div class="discount-circle discount-circle_small">
				<div class="discount-circle__disc">
					<div class="discount-circle__text">Скидка</div>
					<div class="discount-circle__value">35%</div>
				</div>
				<div class="discount-circle__append">при покупке в рассрочку</div>
			</div>
		</div>

		<h4 class="violet">Название модели</h4>
		<p>Для детей мы&nbsp;рекомендуем современные стили&nbsp;&mdash; от&nbsp;минимализма до&nbsp;мягкой классики. Индивидуальные размеры и&nbsp;формы столов, стеллажей, шкафов, кроватей и&nbsp;других предметов мебели позволят сформировать интерьер, соответствующий занятиям и&nbsp;увлечениям ребенка. Мы&nbsp;предлагаем большой выбор цветовых решений&nbsp;&mdash; от&nbsp;ярких тонов до&nbsp;полутонов и&nbsp;текстур&nbsp;&mdash; глянцевых, матовых, древесных и&nbsp;текстильных.
		Это позволяет создать интерьер для ребенка любого возраста и&nbsp;характера.</p>
		<p>Цвет корпуса&nbsp;&mdash; Дуб Шато<br>
		Фасады&nbsp;&mdash; ЛДСП, пластик Abet Laminati, эмаль</p>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/wholesale']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Оптовые продажи
			</a>
		</div>
	</div>
</div>
