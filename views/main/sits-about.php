<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Корпусная мебель:<br> Готовые решения';
?>

<?= $this->render('@parts/pagination-sits') ?>

<script>
	document.body.className = 'violet-phone hide-logo';
</script>
<style>
	body {
		background-color: #f7f7f7;
	}
</style>

<div class="row h100 padding-top-mobile">
	<div class="col-sm-6 h100">
		<div class="h50 expand-to-gutters">
			<iframe id="drive-viewer-video-player-object-0" frameborder="0" allowfullscreen="1" title="Видеоплеер" width="100%" height="100%" src="https://youtube.googleapis.com/embed/?status=ok&hl=ru&allow_embed=0&ps=docs&partnerid=30&autoplay=0&docid=0B5_kppMKX3nudUxrcURJeWFIWk0&abd=0&public=true&el=preview&title=%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BE%D1%82%D0%B2%D0%B5%D1%82%20%D0%BE%D1%82%2002.11.2014%20%D0%B3.%20-%20%D0%9A%D1%83%D1%85%D0%BD%D1%8F%20%D1%81%20%D0%B7%D0%B8%D0%BC%D0%BD%D0%B8%D0%BC%20%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6%D0%B5%D0%BC.mp4&BASE_URL=https%3A%2F%2Fdrive.google.com%2F&iurl=https%3A%2F%2Fdrive.google.com%2Fvt%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0%26s%3DAMedNnoAAAAAVaNvUWXYuByRuy720_BLHVYfd83k2QnT&cc3_module=1&reportabuseurl=https%3A%2F%2Fdrive.google.com%2Fabuse%3Fauthuser%3D0%26id%3D0B5_kppMKX3nudUxrcURJeWFIWk0&token=1&plid=V0QT8Gv-8WIAsw&timestamp=1436767025257&length_seconds=1841&BASE_YT_URL=https%3A%2F%2Fdrive.google.com%2F&cc_load_policy=1&authuser=0&wmode=window&override_hl=1&enablecastapi=0&enablejsapi=1&origin=https%3A%2F%2Fdrive.google.com"></iframe>
		</div>

		<div class="kitchens-intro__choose h50">
			<h3 class="violet">Выберите мебель сейчас</h3>
			<p>Оставьте свой номер телефона и&nbsp;в&nbsp;ближайшее время с&nbsp;вами свяжется наш специалист, который поможет составить дизайн вашей будущей кухни в&nbsp;удобное для вас время совершенно бесплатно!</p>

			<?= $this->render('@parts/form-callback') ?>
		</div>
	</div>
	<div class="col-sm-6 block-padding-top h100">
		<h1 class="furniture-ready-cilek__title sits__title">
			Мягкая мебель
		</h1>

		<ul class="list list_black">
			<li>20 лет на&nbsp;рынке</li>
			<li>700&nbsp;000 диванов и&nbsp;кресел ручной работы</li>
			<li>Продается в&nbsp;32 странах мира</li>
			<li>Эстетика, комфорт, тренд&nbsp;&mdash; основные ценности фабрики</li>
			<li>Вся продукция соответствует европейскому классу гигиеничности Е1</li>
		</ul>

		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/contacts']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Корпусная мебель на заказ
			</a>
		</div>
	</div>
</div>
