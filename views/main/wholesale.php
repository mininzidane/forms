<?php
/**
 * @var $this        yii\web\View
 * @var $mailSuccess bool
 * */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Оптовые продажи';
?>

<script>
	document.body.className = 'light-body';
</script>
<style>
	body {
		background-color: #f0f3f8;
	}
</style>

<div class="h50">
	<h1 class="violet title title-margin-top"><?= $this->title ?></h1>
	<p>Осуществляем продажи оптом для: баров, ресторанов, кафе, гостиниц, отелей, казино, <nobr>бизнес-центорв</nobr>, детских садов, школ, поликлиник и&nbsp;много другого. А&nbsp;также огромный
	ассортимент аксессуаров (часы, вазы, текстиль, ростовые зеркала, светильники, статуэтки)</p>
	<p><a class="wholesale__mail" href="mailto:opt@formsvl.ru">opt@formsvl.ru</a></p>
	<p class="wholesale__phone">+7 914 736 35 25</p>
</div>

<div class="row h50">
	<div class="col-sm-6 h100 how-we-work__green-block">
		<h3 class="uc white">Достаточно указать имя и телефон, и мы с вами свяжемся в ближайшее время</h3>

		<form method="post" action="" class="form-callback wholesale__callback" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<input class="form-control form-callback__text" type="text" name="name" placeholder="Имя">
					</div>
					<div class="form-group">
						<input required class="form-control form-callback__text" type="text" name="phone" placeholder="Телефон">
					</div>
					<div class="form-group">
						<label class="form-callback__file">
							Прикрепить файл
							<input type="file" name="file" style="display: none">
							<span class="glyphicon glyphicon-paperclip form-callback__file-icon"></span>
						</label>
					</div>
				</div>
				<div class="col-sm-6 form-group">
					<textarea name="comment" class="form-control form-callback__text" placeholder="Комментарий"></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<button class="btn btn-summary form-callback__button">Отправить запрос</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-sm-6 h100">
		<div class="pin-to-bottom pin-to-bottom_w50">
			<a href="<?= Url::to(['main/designers']) ?>"
			   class="text-horizontal-arrow text-horizontal-arrow_violet uc">
				<span class="arrowed-link__arrow"></span>
				Дизайнерам посвящается
			</a>
		</div>
	</div>
</div>
<script>
	<?php if ($mailSuccess !== null) {
		echo 'alert("' . ($mailSuccess? 'Сообщение отправлено': 'Ошибка отправки') . '");';
	} ?>
</script>