<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Сгостиные, детские, спальни по цене, напрямую  от производителя';
?>

<script>
	document.body.className = 'light-body';
</script>

<style>
	body {
		background-color: #f0f3f8;
	}
</style>

<ul class="actions__nav">
	<li class="active">
		<a data-toggle="tab" href="#tv" class="text-horizontal-arrow">
			Телевизор в&nbsp;подарок
			<span class="arrowed-link__arrow arrowed-link__arrow_left"></span>
		</a>
	</li>
	<li>
		<a data-toggle="tab" href="#fridge" class="text-horizontal-arrow">
			Встраиваемый холодильник в&nbsp;подарок
			<span class="arrowed-link__arrow arrowed-link__arrow_left"></span>
		</a>
	</li>
	<li>
		<a data-toggle="tab" href="#dish-washer" class="text-horizontal-arrow">
			Встраиваемая посудомоечная машина в&nbsp;подарок
			<span class="arrowed-link__arrow arrowed-link__arrow_left"></span>
		</a>
	</li>
</ul>

<div class="row h100">
	<div class="col-sm-offset-2 col-sm-8 h100" style="overflow: hidden;">
		<h1 class="title-benefits title-benefits_violet">Сгостиные, детские, спальни
			<span class="yellow-bg">по&nbsp;цене, напрямую</span> от&nbsp;производителя</h1>

		<p class="t-C">При покупке мебели VOX, Paged, Glek, Flippy Grandy.</p>

		<div class="tab-content t-C">
			<div class="tab-pane active" id="tv">
				<div class="actions__title">
					<span class="actions__title-tail"></span>
					<span class="actions__title-tail actions__title-tail_right"></span>
					Телевизор в&nbsp;подарок
				</div>

				<div class="t-C">
					<img class="actions__img" src="/images/tv-big-action.png" alt="">
				</div>

				<p>Телевизор LG. Новинка 2015 года!</p>
			</div>
			<div class="tab-pane" id="fridge">
				<div class="actions__title">
					<span class="actions__title-tail"></span>
					<span class="actions__title-tail actions__title-tail_right"></span>
					Холодильник в&nbsp;подарок
				</div>

				<div class="t-C">
					<img class="actions__img" src="/images/ice-big-action.png" alt="">
				</div>

				<p>Холодильник LG. Новинка 2015 года!</p>
			</div>
			<div class="tab-pane" id="dish-washer">
				<div class="actions__title">
					<span class="actions__title-tail"></span>
					<span class="actions__title-tail actions__title-tail_right"></span>
					Посудомойка в&nbsp;подарок
				</div>

				<div class="t-C">
					<img class="actions__img" src="/images/wash-big-action.png" alt="">
				</div>

				<p>Посудомоечная машина LG. Новинка 2015 года!</p>
			</div>
		</div>

		<div class="main-menu__credit actions__credit">
			<i></i>
			<a href="<?= Url::to(['main/credit']) ?>">Действует рассрочка от&nbsp;салона «Forms»</a>
		</div>

		<p class="t-C">подробности по&nbsp;телефону +7&nbsp;423 20 90&nbsp;999 или у&nbsp;сотрудников салона</p>
	</div>

	<?= $this->render('@parts/button-back') ?>
</div>