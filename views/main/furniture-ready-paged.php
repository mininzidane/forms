<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Корпусная мебель:<br> готовые решения';
?>

<?= $this->render('@parts/furniture-ready-paged') ?>

<script>
	document.body.className = 'furniture-ready-paged__bg';
</script>

<h1 class="title-benefits"><?= $this->title ?></h1>

<div class="furniture-ready__desc t-C">
	(массив натурального дерева)
</div>

<div class="row">
	<div class="col-sm-offset-1 col-sm-5 t-C">
		<a href="<?= Url::to(['pdf/paged-1']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/paged-pdf1.png" alt="">
			<span>Каталог мебели PAGED</span>
		</a>
	</div>
	<div class="col-sm-5 t-C">
		<a href="<?= Url::to(['pdf/paged-2']) ?>" target="_blank" class="kitchens__pdf-links" data-disable-ajax>
			<img src="/images/paged-pdf2.png" alt="">
			<span>Каталог столов и стульев PAGED</span>
		</a>
	</div>
</div>

<div class="arrowed-link pin-to-bottom margin-bottom">
	<a class="arrowed-link__link" href="<?= Url::to(['main/furniture-ready-paged-about']) ?>">
		<span class="arrowed-link__text">Мебель Paged</span>
		<span class="arrowed-link__arrow"></span>
	</a>
	<?= $this->render('@parts/order-fitting') ?>
</div>