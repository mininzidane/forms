<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use Swift_Attachment;

class MainController extends Controller {

	public $enableCsrfValidation = false;

	public function beforeAction($action) {
		if (!parent::beforeAction($action)) {
			return false;
		}

		if (Yii::$app->request->isAjax) {
			$this->layout = 'ajax';
		}
		return true;
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
		];
	}

	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST? 'testMe': null,
			],
		];
	}

	// INTRO

	public function actionIndex() {
		return $this->render('index');
	}

	public function actionCatalog() {
		return $this->render('catalog');
	}

	public function actionBenefits() {
		return $this->render('benefits');
	}

	public function actionWhatToKnow() {
		return $this->render('what-to-know');
	}

	public function actionAbout() {
		return $this->render('about');
	}

	public function actionHowWeWork() {
		return $this->render('how-we-work');
	}

	// KITCHENS

	public function actionKitchensPdf() {
		return $this->render('kitchens-pdf');
	}

	public function actionKitchensIntro() {
		return $this->render('kitchens-intro');
	}

	public function actionKitchensBenefits() {
		return $this->render('kitchens-benefits');
	}

	public function actionKitchensTv() {
		return $this->render('kitchens-tv');
	}

	public function actionKitchensMap() {
		return $this->render('kitchens-map');
	}

	// FURNITURE READY

	public function actionFurnitureReady() {
		return $this->render('furniture-ready');
	}

	public function actionFurnitureReadyAbout() {
		return $this->render('furniture-ready-about');
	}

	public function actionFurnitureReadyPaged() {
		return $this->render('furniture-ready-paged');
	}

	public function actionFurnitureReadyPagedAbout() {
		return $this->render('furniture-ready-paged-about');
	}

	public function actionFurnitureReadyCilek() {
		return $this->render('furniture-ready-cilek');
	}

	public function actionFurnitureReadyCilekAbout() {
		return $this->render('furniture-ready-cilek-about');
	}

	public function actionFurnitureReadyCilekMap() {
		return $this->render('furniture-ready-cilek-map');
	}

	public function actionFurnitureReadyCilekYears() {
		return $this->render('furniture-ready-cilek-years');
	}

	public function actionFurnitureReadyTctNanotec() {
		return $this->render('furniture-ready-tct-nanotec');
	}

	public function actionFurnitureOrder() {
		return $this->render('furniture-order');
	}

	public function actionFurnitureOrderAbout() {
		return $this->render('furniture-order-about');
	}

	public function actionSits() {
		return $this->render('sits');
	}

	public function actionSitsAbout() {
		return $this->render('sits-about');
	}

	// INFO PAGES

	public function actionContacts() {
		return $this->render('contacts');
	}

	public function actionSampleSales() {
		return $this->render('sample-sales');
	}

	public function actionWholesale() {

		$mailSuccess = null;

		if (Yii::$app->request->isPost) {
			$params = Yii::$app->request->post();

			$mailSuccess = Yii::$app->mailer
				->compose('@views/mail/common', [
					'name'    => $params['name'],
					'phone'   => $params['phone'],
					'comment' => $params['comment'],
				])
				->attach($_FILES['file']['tmp_name'], ['fileName' => $_FILES['file']['name']])
				->setFrom(Yii::$app->params['emailFrom'])
				->setTo(Yii::$app->params['emailTo'])
				->setSubject('Заказ звонка с сайта ' . Yii::$app->name)
				->send();
		}

		return $this->render('wholesale', ['mailSuccess' => $mailSuccess]);
	}

	public function actionDesigners() {
		return $this->render('designers');
	}

	public function actionDreamJob() {
		return $this->render('dream-job');
	}

	public function actionActions() {
		return $this->render('actions');
	}

	public function actionCredit() {
		return $this->render('credit');
	}
}
