<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class AjaxController extends Controller {

	public $enableCsrfValidation = false;
	public $layout = 'ajax';

	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionSendEmail() {
		$success = false;
		if ($params = Yii::$app->request->post()) {
			$success = Yii::$app->mailer
				->compose('@views/mail/common', [
					'name'  => $params['name'],
					'phone' => $params['phone']
				])
				->setFrom(Yii::$app->params['emailFrom'])
				->setTo(Yii::$app->params['emailTo'])
				->setSubject('Заказ звонка с сайта ' . Yii::$app->name)
				->send();
		}

		echo (int) $success;
	}
}
