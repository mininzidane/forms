<?php

$productionEnvName = 'prod';
$env = isset($_SERVER['APPLICATION_MODE']) ? $_SERVER['APPLICATION_MODE'] : $productionEnvName;

defined('APPLICATION_MODE') or define('APPLICATION_MODE', $env);
defined('YII_DEBUG') or define('YII_DEBUG', APPLICATION_MODE != $productionEnvName);

// -- mapping for YII predefined environment constants (values: prod, dev, test)
if (!in_array($env, [$productionEnvName, 'dev', 'test'])) {
	$env = $productionEnvName;
}
defined('YII_ENV') or define('YII_ENV', $env);
// -- -- -- --

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

// calculate filename for config: use your own config file defined in environment variable APPLICATION_MODE
$configPath = __DIR__ . '/../config';
$config = require($configPath .'/_main.php');
if (file_exists($configPath .'/.env.php')) {
	$config = array_replace_recursive($config, require($configPath .'/.env.php'));
}

(new yii\web\Application($config))->run();
