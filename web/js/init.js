var libsDirectory = byId('main-js')
	.getAttribute('data-assets-base-url');

requirejs.config({
	baseUrl: '/js',
	paths: {
		text: libsDirectory + 'requirejs-text/text',
		jquery: libsDirectory + 'jquery/dist/jquery',
		backbone: libsDirectory + 'backbone/backbone',
		underscore: libsDirectory + 'underscore/underscore',
		bootstrap: libsDirectory + 'bootstrap/dist/js/bootstrap',
		mousewheel: libsDirectory + 'jquery-mousewheel/jquery.mousewheel'
	},
	shim: {
		'underscore': {
			exports: '_'
		},
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		bootstrap: {
			deps: ['jquery']
		}
	}
});

require(
	['jquery', 'app', 'bootstrap', 'mousewheel'],
	function ($, app, bootstrap, mousewheel) {
		app.run();
	}
);

function byId(id) {
	return document.getElementById(id);
}

function isEmpty(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			return false;
		}
	}
	return true;
}