define([
	'jquery',
	'backbone',
	'modules/show-by-hash',
	'modules/order-fitting-form',
	'modules/kitchens-map',
	'modules/ya-maps'
], function ($, Backbone, ShowByHash, FittingForm, KitchensMap, YaMaps) {

	var afterLoadDeferred;

	var Routes = Backbone.Router.extend({

		currentView: null,

		afterLoad: function(callback) {
			var that = this;

			if (this.currentView) {
				this.currentView.undelegateEvents();
			}

			$.when(afterLoadDeferred.promise())
				.done(function() {
					callback.call(that)
				});
		},

		routes: {
			'how-we-work/': 'fittingForm',
			'kitchens-map/': 'kitchensMap',
			'furniture-ready-about/': 'fittingForm',
			'kitchens-benefits/': 'fittingForm',
			'kitchens-intro/': 'fittingForm',
			'kitchens-tv/': 'kitchensTv',
			'furniture-ready-cilek-map/': 'kitchensMap',
			'contacts/': 'yaMaps'
		},
		fittingForm: function() {
			var that = this;
			this.afterLoad(function() {
				that.currentView = new FittingForm();
			});
		},
		kitchensMap: function() {
			var that = this;
			this.afterLoad(function() {
				that.currentView = new KitchensMap();
			});
		},
		kitchensTv: function() {
			var that = this;
			this.afterLoad(function() {
				that.currentView = new ShowByHash();
			});
		},
		yaMaps: function() {
			var that = this;
			this.afterLoad(function() {
				that.currentView = new YaMaps();
			});
		}
	});

	var Application = Backbone.View.extend({

		el: '#app',

		/**
		 * Activate main-menu links
		 *
		 * @param {string} currentUrl
		 */
		activateMenuChapter: function(currentUrl) {
			var chapters = $('body').data('main-menu-chapters');

			var itemUrl = currentUrl;
			if (!chapters.hasOwnProperty(itemUrl)) {
				var chapterItems;
				for (var chapter in chapters) {
					chapterItems = chapters[chapter];
					if ($.inArray(itemUrl, chapterItems) != -1) {
						itemUrl = chapter;
						break;
					}
				}
			}

			var $menuItems = $('.js-main-menu > li');
			$menuItems.removeClass('active').each(function() {
				var $this = $(this);

				if ($this.children('a').attr('href') == itemUrl) {
					$this.addClass('active');
				}
			});

			return this;
		},

		/**
		 * Insert content to placeholder
		 *
		 * @param {string} url
		 */
		insertContentOnPage: function (url) {
			var $loader = $('.js-ajax-loader').show(),
				that = this;

			afterLoadDeferred = $.Deferred();

			$('#faded').show();

			$.ajax(url, {
				success: function (data) {
					that.$el.html(data);
					$('body').removeClass('slide-up slide-down');

					afterLoadDeferred.resolve();

					that.initTooltip().activateMenuChapter(url);
					routes.navigate(url, {trigger: true});
				},
				complete: function() {
					$loader.hide();
					$('#faded').hide();
				},
				error: function() {
					afterLoadDeferred.reject();
				}
			});
		},

		/**
		 * We need body at least 100% height from window
		 */
		calculateBodyHeight: function () {
			var smScreen = 768,// bootstrap 'sm' media breakpoint (sm: 480)
				$body = $('body'),
				bodyHeight,
				$window = $(window),
				windowHeight = $window.height();

			if ($window.width() <= smScreen) { // don't set body height for small-screen devices
				$body.height('auto');
				return this;
			}

			$body.height('auto');
			bodyHeight = $body.height();

			var neededBottomSpace = 173,// необходимое пространство для кнопок следующего шага для случая, если высоты окна недостаточно
				offsetForBottomOfThePage = 0;
			if (bodyHeight > windowHeight) {
				offsetForBottomOfThePage = neededBottomSpace;
			}

			$body.height(Math.max(windowHeight, bodyHeight) + offsetForBottomOfThePage);

			return this;
		},

		/**
		 * Click event for using ajax to load content
		 *
		 * @param {object} e JS event
		 */
		clicker: function (e) {
			e.preventDefault();
			var url = $(e.currentTarget).attr('href');
			this.insertContentOnPage(url);
		},

		initTooltip: function() {
			// init tooltips
			$('[data-toggle="tooltip"]').tooltip();
			return this;
		},

		initMouseWheel: function() {
			var scrollDelay = 1500,// delay in ms to detect a single scroll. Bigger value means that scroll event is 'longer' for each scroll action
				that = this,
				$body = $('body'),
				timeStamp = new Date().getTime(),
				menuChapters = $body.data('main-menu-chapters'),
				linearUrls = [];

			$.each(menuChapters, function (i, item) {
				linearUrls.push(i);
				linearUrls = linearUrls.concat(item)
			});

			$body.on('mousewheel', function (event) {
				var timeNow = new Date().getTime(),
					scrollUp = event.deltaY > 0,
					linearUrlIndex = linearUrls.indexOf(location.pathname);
				event.preventDefault();

				// If the last mousescroll happened less that {scrollDelay} ms ago, update timeStamp and do nothing
				if (timeNow - timeStamp < scrollDelay || (linearUrlIndex == linearUrls.length - 1 && !scrollUp) || (linearUrlIndex == 0 && scrollUp)) {
					return;
				}
				timeStamp = timeNow;
				//console.log(event.deltaX, event.deltaY, event.deltaFactor);

				$body.addClass(scrollUp? 'slide-down': 'slide-up');

				var targetUrl = scrollUp? linearUrls[linearUrlIndex - 1]: linearUrls[linearUrlIndex + 1];
				that.insertContentOnPage(targetUrl);
			});
		},

		initialize: function () {
			var that = this;
			afterLoadDeferred = $.Deferred();

			// make body height adaptive for window height
			this.calculateBodyHeight();
			$(window).on('resize', this.calculateBodyHeight);

			$('body')
				// toggle menu
				.on('click', '.js-menu-toggle', function() {
					$('[data-menu-target]').fadeToggle();
					return false;
				})
				// internal links (no hash)
				.on('click', 'a:not([href ^= "#"]):not([href ^= "http"]):not([data-disable-ajax])', function() {
					var $this = $(this);
					that.insertContentOnPage($this.attr('href'));
					return false;
				});

			// update content on change url
			window.onpopstate = function () {
				that.insertContentOnPage(location.pathname);
			};

			afterLoadDeferred.resolve();
			this.initTooltip().initMouseWheel();
		},

		events: {
			'click a:not([href ^= "#"]):not([href ^= "http"]):not([data-disable-ajax])': 'clicker'
		},

		run: function () {
			Backbone.history.start({pushState: true});
			this.render();
		}

	});

	var routes = new Routes();
	var app = new Application({
		routes: routes
	});

	return app;
});