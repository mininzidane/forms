define([
	'jquery',
	'backbone'
], function ($, Backbone) {
	return Backbone.View.extend({
		el: '#app',
		events: {
			'click a.js-show-by-hash, .js-show-by-hash a': 'showByHash'
		},
		showByHash: function(e) {
			event.preventDefault();
			var $this = $(e.currentTarget),
				hash = $this.attr('href');

			$('[data-show-by-hash-group]').hide();
			$(hash).show();

			$('a.js-show-by-hash, .js-show-by-hash a').removeClass('active');
			$this.addClass('active');
		}
	});
});