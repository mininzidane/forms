define([
	'jquery',
	'backbone'
], function ($, Backbone) {
	return Backbone.View.extend({
		el: '#YMapsID',
		initialize: function() {
			var that = this;

			YMaps.load(function() {
				// Создает обработчик события window.onLoad
				YMaps.jQuery(function () {
					// Создает экземпляр карты и привязывает его к созданному контейнеру
					var map = new YMaps.Map(YMaps.jQuery(that.el)[0]);
					var longitude = 131.909119,
						latitude = 43.146097;

					// Устанавливает начальные параметры отображения карты: центр карты и коэффициент масштабирования
					map.setCenter(new YMaps.GeoPoint(longitude, latitude), 15);

					var placemark = new YMaps.Placemark(new YMaps.GeoPoint(longitude, latitude));
					placemark.name = "Forms";
					placemark.description = "г. Владивосток, ул. Ильичева, 4<br> (ЖК «Аркада Хаус»)";
					map.addOverlay(placemark);
				});
			});
		}
	});
});