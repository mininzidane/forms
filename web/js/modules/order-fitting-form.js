define([
	'jquery',
	'backbone'
], function ($, Backbone) {
	return Backbone.View.extend({
		el: '#app',
		events: {
			'click .js-show-form': 'showForm',
			'submit .js-ajax-submit': 'submitForm'
		},
		showForm: function(e) {
			e.preventDefault();
			var hash = $(e.currentTarget).attr('href');
			$(hash).show();
			$('[data-callback-form-intro]').hide();
			$('.how-we-work__propose').addClass('how-we-work__propose_form');
		},
		submitForm: function(e) {
			e.preventDefault();
			//this.undelegateEvents();
			var $this = $(e.currentTarget);
			$.ajax({
				url: $this.attr('action'),
				data: $this.serialize(),
				type: $this.attr('method') || 'get',
				success: function (data) {
					if (parseInt(data)) {
						alert('Сообщение отправлено');
					} else {
						alert('Ошибка отправки');
					}
				}
			});
		}
	});
});