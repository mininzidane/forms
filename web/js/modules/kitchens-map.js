define([
	'jquery',
	'backbone'
], function ($, Backbone) {
	return Backbone.View.extend({
		el: '.js-kitchen-map',
		events: {
			'click .map-pin': 'activateMapPin'
		},
		activateMapPin: function(e) {
			$(e.currentTarget).toggleClass('active');
		},
		initialize: function() {
			this.$el.tooltip({
				placement: 'auto bottom',
				selector: '.map-pin',
				trigger: 'click',
				template: '<div class="tooltip tooltip-map-pin" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
				html: true
			});
		}
	});
});